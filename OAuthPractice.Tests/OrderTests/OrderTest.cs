﻿using FluentAssertions;
using OAuthPractice.ProtectedApi.Models;
using Xunit;

namespace OAuthPractice.Tests.OrderTests
{
    public class OrderTest:ApiTestsBase
    {
        [Fact]
        public void Should_get_orders()
        {
            var orders = ApiInvoker.InvokeGetRequest<Order>("Orders");

            orders.Should().Be("");
        }
    }
}